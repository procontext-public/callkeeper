<?php

namespace Procontext\CallKeeper;

use Procontext\CallKeeper\Exception\CallKeeperDisabledException;
use Procontext\CallKeeper\Exception\CallKeeperResponseException;

class CallKeeper
{
    const REQUEST_URL = 'https://api.callkeeper.ru/orderCall';

    const RESPONSE_SUCCESS = 'success';
    const RESPONSE_FAILED = 'fail';

    private $enable;

    public function __construct()
    {
        $this->enable = env('CK_ENABLE', false);
    }

    public function send(CallKeeperParams $params): array
    {
        if(!$this->isEnable()) {
            throw new CallKeeperDisabledException();
        }
        
        $curl = curl_init(self::REQUEST_URL);
        curl_setopt_array($curl, [
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query($params->export()),
            CURLOPT_CONNECTTIMEOUT => 30
        ]);

        $response = json_decode(curl_exec($curl), true);
        $response = isset($response[0]) && !empty($response[0])
            ? $response[0] :
            ['status' => self::RESPONSE_FAILED, 'reason' => curl_error($curl)];

        curl_close($curl);

        $status = isset($response['status']) ? $response['status'] : self::RESPONSE_FAILED;

        if($status !== self::RESPONSE_SUCCESS) {
            throw new CallKeeperResponseException($response);
        }

        return [
            'status' => isset($response['status']) ? $response['status'] : self::RESPONSE_FAILED,
            'id' => isset($response['id']) ? $response['id'] : '',
            'manager' => isset($response['manager']) ? $response['manager'] : '',
            'client' => isset($response['client']) ? $response['client'] : '',
            'reason' => isset($response['reason']) ? $response['reason'] : '',
        ];
    }

    public function isEnable(): bool
    {
        return $this->enable;
    }
}
