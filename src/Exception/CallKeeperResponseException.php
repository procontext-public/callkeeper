<?php

namespace Procontext\CallKeeper\Exception;

use Throwable;

class CallKeeperResponseException extends CallKeeperException
{
    protected $response;

    public function __construct($response = [], $message = 'Ошибка ответа CallKeeper API', $code = 500, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    public function getResponse(): array
    {
        return $this->response;
    }
}